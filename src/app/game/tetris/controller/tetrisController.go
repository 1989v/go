package controller

import (
	"net/http"

	"github.com/labstack/echo"
	tetrisService "got.kr/go/src/app/game/tetris/service"
)

var model = make(map[string]interface{})

func TetrisController(e *echo.Echo) {
	g := e.Group("/game/tetris")

	/**
	 *	mapping : /game/tetris
	 */
	g.GET("", func(c echo.Context) (err error) {
		tetris := tetrisService.Index(c)

		model["tetris"] = tetris
		return c.Render(http.StatusOK, "tetris", model)
	})

	/**
	 *	mapping : /game/tetris/api/control
	 */
	g.POST("/api/control", func(c echo.Context) (err error) {
		tetris := tetrisService.Control(c)

		return c.JSON(http.StatusOK, tetris)
	})
}
