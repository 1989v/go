package service

import (
	"reflect"

	"github.com/labstack/echo"
	tetrisModel "got.kr/go/src/app/game/tetris/model"
)

/* service */
func Index(c echo.Context) tetrisModel.Tetris {
	tetris := modelInit(c)

	return tetris
}

/* service */
func Control(c echo.Context) tetrisModel.Tetris {
	tetris := modelInit(c)

	if tetris.Common.KeyCode == 13 {
		// game start
		tetris.NowBlock = makeRandomBlock(tetris)
		tetris.NextBlock = makeRandomBlock(tetris)

		// game start flag set
		tetris.Common.IsStarted = 1
	} else if tetris.Common.KeyCode == 37 {
		// move left
		tetris = moveBlock(tetris, false)
	} else if tetris.Common.KeyCode == 39 {
		// move right
		tetris = moveBlock(tetris, false)
	} else if tetris.Common.KeyCode == 40 {
		// move down
		tetris = moveBlock(tetris, false)
	} else if tetris.Common.KeyCode == 38 {
		// rotate block
		tetris = rotateBlock(tetris)
	}

	// Draw Now Block Canvas Data
	if tetris.NowBlock != nil {
		tetris = convertCanvasData(tetris)
	}

	return tetris
}

/**
 *	function : Model Init
 */
func modelInit(c echo.Context) tetrisModel.Tetris {
	tetris := new(tetrisModel.Tetris)
	if err := c.Bind(tetris); err != nil {
		c.Logger().Error(err)
	}

	tetris = tetrisModel.DefaultInit(tetris)

	return *tetris
}

/**
 *	function : Data converter, tetris block data convert to canvas data
 */
func convertCanvasData(tetris tetrisModel.Tetris) tetrisModel.Tetris {
	var blockKey string
	var blockLineFixelCnt int
	var centerBlockPosition int
	var convertedCanvasBlockData []int
	var convertedCanvasBlockDataArray [][]int

	// set block common values
	blockKey = reflect.ValueOf(tetris.NowBlock).MapKeys()[0].Interface().(string)

	// set block data
	blockLineFixelCnt = len(tetris.Blocks[blockKey][tetris.NowBlock[blockKey]]) / 4
	centerBlockPosition = tetris.Width/2 - blockLineFixelCnt/2

	// When Game Start, set Now Block Starting Default Position
	if tetris.NowBlockCanvasData == nil {
		tetris.NowBlockPositionX = centerBlockPosition
		tetris.NowBlockPositionY = 0
	}

	// converting
	nowBlockCanvasData := make(map[string][][]int)
	for i, blockFixelFlag := range tetris.Blocks[blockKey][tetris.NowBlock[blockKey]] {
		var blockPositionHeight int
		if i > 3 {
			blockPositionHeight = tetris.BlockHeight * (i / 4)
		}
		if blockFixelFlag == 1 {
			// set brick position
			x := (i%blockLineFixelCnt)*tetris.BlockWidth + tetris.NowBlockPositionX*tetris.BlockWidth
			y := blockPositionHeight + tetris.NowBlockPositionY*tetris.BlockHeight

			// when this brick location is outside the width area, then do return
			if x < 0 || x > (tetris.Width-1)*tetris.BlockWidth || y < 0 {
				// move position rollback
				tetris = moveBlock(tetris, true)
				return tetris
			}

			// when typing the keyboard down arrow key and this brick overlap StackedBlock, then stack this block
			if isStacked(tetris.StackedBlockCanvasData, []int{x, y}) {
				if tetris.Common.KeyCode == 40 {
					tetris = stackBlock(tetris)
					tetris = convertCanvasData(tetris)
					tetris = moveBlock(tetris, true)
					return tetris
				} else {
					tetris = moveBlock(tetris, true)
					return tetris
				}
			}

			// when this brick location is outside the height area, then stack this block
			if y > (tetris.Height-1)*tetris.BlockHeight {
				tetris = stackBlock(tetris)
				tetris = convertCanvasData(tetris)
				tetris = moveBlock(tetris, true)
				return tetris
			}

			// line clear check
			if tetris.StackedBlockCanvasData != nil {
				tetris = lineClearCheck(tetris)
			}

			convertedCanvasBlockData = []int{x, y}
		}

		convertedCanvasBlockDataArray = append(convertedCanvasBlockDataArray, convertedCanvasBlockData)
	}
	nowBlockCanvasData[blockKey] = convertedCanvasBlockDataArray
	tetris.NowBlockCanvasData = nowBlockCanvasData

	// Unnecessary Data nil Set
	tetris.Blocks = nil

	return tetris
}

func isStacked(map1 map[string][][]int, value []int) bool {
	flag := false
	for _, v := range map1 {
		for _, sub := range v {
			if len(sub) == len(value) && sub[0] == value[0] && sub[1] == value[1] {
				flag = true
			}
		}
	}
	return flag
}

/**
 *	function : stack tetris block
 */
func stackBlock(tetris tetrisModel.Tetris) tetrisModel.Tetris {

	blockKey := reflect.ValueOf(tetris.NowBlockCanvasData).MapKeys()[0].Interface().(string)
	//tetris.StackedBlockCanvasData = map[string][][]int{blockKey: tetris.NowBlockCanvasData[blockKey]}
	if tetris.StackedBlockCanvasData != nil {
		for i := range tetris.NowBlockCanvasData[blockKey] {
			tetris.StackedBlockCanvasData[blockKey] = append(tetris.StackedBlockCanvasData[blockKey], tetris.NowBlockCanvasData[blockKey][i])
		}
	} else {
		tetris.StackedBlockCanvasData = map[string][][]int{blockKey: tetris.NowBlockCanvasData[blockKey]}
	}

	tetris.NowBlockCanvasData = nil
	if tetris.NextBlock != nil {
		tetris.NowBlock = tetris.NextBlock
	} else {
		tetris.NowBlock = makeRandomBlock(tetris)
	}
	tetris.NextBlock = makeRandomBlock(tetris)

	return tetris
}

/**
 *	function : make random tetris block
 */
func makeRandomBlock(tetris tetrisModel.Tetris) map[string]int {
	var blockKey string = reflect.ValueOf(tetris.Blocks).MapKeys()[0].Interface().(string)

	return map[string]int{
		blockKey: 0,
	}
}

/**
 *	function : move tetris block position
 */
func moveBlock(tetris tetrisModel.Tetris, returnFlag bool) tetrisModel.Tetris {

	if tetris.Common.KeyCode == 37 {
		// move left
		if !returnFlag {
			tetris.NowBlockPositionX = tetris.NowBlockPositionX - 1
		} else {
			tetris.NowBlockPositionX = tetris.NowBlockPositionX + 1
		}

	} else if tetris.Common.KeyCode == 39 {
		// move right
		if !returnFlag {
			tetris.NowBlockPositionX = tetris.NowBlockPositionX + 1
		} else {
			tetris.NowBlockPositionX = tetris.NowBlockPositionX - 1
		}

	} else if tetris.Common.KeyCode == 40 {
		// move down
		if !returnFlag {
			tetris.NowBlockPositionY = tetris.NowBlockPositionY + 1
		} else {
			tetris.NowBlockPositionY = tetris.NowBlockPositionY - 1
		}
	}

	return tetris
}

/**
 *	function : rotate tetris block
 */
func rotateBlock(tetris tetrisModel.Tetris) tetrisModel.Tetris {
	blockKey := reflect.ValueOf(tetris.NowBlock).MapKeys()[0].Interface().(string)
	blockShapeIdx := tetris.NowBlock[blockKey]
	blockShapeCnt := len(tetris.Blocks[blockKey])

	if blockShapeIdx < (blockShapeCnt - 1) {
		blockShapeIdx++
	} else {
		blockShapeIdx = 0
	}

	tetris.NowBlock[blockKey] = blockShapeIdx

	return tetris
}

/**
 *	function : line clear check
 */
func lineClearCheck(tetris tetrisModel.Tetris) tetrisModel.Tetris {
	clearCounter := 0

	lineChecker := make(map[int][]int)
	for _, v := range tetris.StackedBlockCanvasData {
		for _, brick := range v {
			// 해당 y행의 map객체 유무 확인
			if brick != nil {
				if brickValueArray, ok := lineChecker[brick[1]]; ok {
					// map 객체에 brick값 유무 확인하여 없을경우
					if ok2, _ := in_array(brick[0], brickValueArray); !ok2 {
						// 현재 x좌표 입력
						lineChecker[brick[1]] = append(lineChecker[brick[1]], brick[0]) //[]int{brick[0]}

						brickValueArrayCount := len(lineChecker[brick[1]])
						if brickValueArrayCount >= tetris.Width {
							tetris = clearLine(tetris, brick[1])
							clearCounter++
						}
					}

					// 해당 y행 map객체가 없을 경우 생성
				} else {
					lineChecker[brick[1]] = append(lineChecker[brick[1]], brick[0])
				}
			}
		}
	}

	tetris.Score = tetris.Score + clearCounter

	return tetris
}

/**
 *	function : clear tetris line
 */
func clearLine(tetris tetrisModel.Tetris, lineY int) tetrisModel.Tetris {
	// lineY에 해당하는 벽돌을 지운 후의 데이터
	newStackedBlockData := make(map[string][][]int)

	for blockKey, v := range tetris.StackedBlockCanvasData {
		var stackedBlockBrickByBlockKey [][]int
		for _, brick := range v {
			if brick != nil {
				if brick[1] != lineY {
					// lineY보다 위에있던 벽돌들의 높이값 조정
					if brick[1] < lineY {
						brick[1] = brick[1] + tetris.BlockHeight
					}
					stackedBlockBrickByBlockKey = append(stackedBlockBrickByBlockKey, []int{brick[0], brick[1]})
				}
			}
		}
		newStackedBlockData[blockKey] = stackedBlockBrickByBlockKey
	}

	tetris.StackedBlockCanvasData = newStackedBlockData

	return tetris
}

/** COMMON FUNC */
func in_array(v interface{}, in interface{}) (ok bool, i int) {
	val := reflect.Indirect(reflect.ValueOf(in))
	switch val.Kind() {
	case reflect.Slice, reflect.Array:
		for ; i < val.Len(); i++ {
			if ok = v == val.Index(i).Interface(); ok {
				return
			}
		}
	}
	return
}
