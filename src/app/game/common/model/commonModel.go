package model

type Common struct {
	// game start flag
	IsStarted int `json:"isStarted"`

	// canvas size
	CtxWidth  int `json:"ctxWidth"`
	CtxHeight int `json:"ctxHeight"`

	// input
	KeyCode int `form:"keyCode" json:"keyCode"`
}
