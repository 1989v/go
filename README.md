# GO

GO 프로젝트

1. 프로젝트 개요

    a. 프로젝트명 : GO TETRIS
    
    b. 프로젝트 설명 : GO로 웹(+앱) 환경의 TETRIS 게임!

    c. 프레임워크 : labstack echo (https://github.com/labstack/echo)


2. 게임 디자인

    ![테트리스_디자인](/uploads/394cb0dd22b5bbbd42ba2fa818dd4faa/테트리스_디자인.jpg)


3. 